// Question # 1 (Warmup)

function enterNumbers1(num1, num2){
	const ans1 = num1 + num2
	const ans2 = ans1 + num2
	const ans3 = ans1 + ans2
	const ans4 = ans2 + ans3
	const ans5 = ans3 + ans4
	const ans6 = ans4 + ans5
	const ans7 = ans5 + ans6
	const ans8 = ans6 + ans7

	console.log(num1);
	console.log(num2);
	console.log(ans1);
	console.log(ans2);
	console.log(ans3);
	console.log(ans4);
	console.log(ans5);
	console.log(ans6);
	console.log(ans7);
	console.log(ans8);
}


// QUESTION #2 (EASY)
// CONVERT DECIMAL TO BINARY USING CONDITIONS AND LOOPS.

function decToBin(num){
	let binary = "";
    let dec = num;
 
    while(dec > 0){
        if(dec % 2 == 0){
            binary = binary + "0";
        }
        else {
            binary = binary + "1";
        }

        dec = Math.floor(dec / 2);
    }

    return binary;
}

// QUESTION #3 (MEDIUM)
// ALL THE OUTPUT NUMBERS MUST BE PRIME FACTORS OF THE ENTERED NUMBER

function primeFactors(num){
	let factors = [];

	for(i = 2; i <= num; i++){
		while((num % i) === 0){
			factors.push(i);
			num /= i;
		}
		
	}

	for(i = 1; i <= factors.length; i++){
		console.log(factors.join("x"))
	}
}

// QUESTION #4 (MEDIUM)
// GET THE SQUARE ROOT OF A NUMBER USING CONDITIONS AND LOOPS.

function squareRoot(num){
	for(i = 0; i * i <= num; i++){
		if (i * i === num){
            return i;
		}else{
			return num;
		}
	}
}

